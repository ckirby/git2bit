require 'date'
require 'github_api'
require 'methadone'

module Git2bit
	class GithubProxy
	  include Methadone::Main
	  include Methadone::CLILogging

		def initialize(args)
			@repo = args[:repo]
			@user = args[:user]
	    @org = args[:org]
	    if @org
	      @repo_owner = @org
	    else
	      @repo_owner = @user
	    end
			begin
  			@conn = Github.new :basic_auth => "#{@user}:#{args[:pass]}"
  			@conn.repos.list user: @user
				info "Successfully connected to Github with user #{@user}"
	    rescue Exception => e 
	      error = ["Error: Unable to connect to Github with user #{@user}", e.message].push e.backtrace
	      exit_now!(error.join("\n"))
	    end
		end

		def each_issue(process_closed = true)
			data = Array.new
			data.push @conn.issues.list :filter => 'all', :state => 'open', :user => @repo_owner, :repo => @repo
			if process_closed
				data.push @conn.issues.list :filter => 'all', :state => 'closed', :user => @repo_owner, :repo => @repo
			end

			data.each do |result_set|
				result_set.each_page do |page|
				  page.each {|issue| yield(issue)}
				end
			end
		end

		def get_comments(issue_id)	    
			begin
				# Get Github comments for issue ID
				comments = @conn.issues.comments.all @repo_owner, @repo, issue_id: issue_id
				debug "Github: got comments for issue ##{issue_id}: " + comments.inspect
	    rescue Exception => e 
	      error "Error: Unable to get comments for Github issue ##{issue_id} - " + e.message
	      debug e.backtrace.join("\n")
	    end
	    comments
		end

	end
end
