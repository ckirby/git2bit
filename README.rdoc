= git2bit - git2bit migrates issues with their comments and milestones from a github repo to a bitbucket repo

Author::  Joe Workman (joe at workmanmail.com)
Copyright:: Copyright (c) 2013 Joe Workman


DESCRIBE YOUR GEM HERE

== Links

* {Source on Bitbucket}[https://bitbucket.org/joeworkman/git2bit]

== Install

Install:
	gem install git2bit

== Shell Examples

Move all issues (open and closed):
	git2bit

Move only open issues: 
	git2bit --no-close

Move a single issue:
	git2bit -i 532
