# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'git2bit/version'

Gem::Specification.new do |gem|
  gem.name          = "git2bit"
  gem.version       = Git2bit::VERSION
  gem.authors       = ["Joe Workman"]
  gem.email         = ["joe at workmanmail.com"]
  gem.description   = %q{git2bit migrates issues with their comments and milestones from a github repo to a bitbucket repo}
  gem.summary       = %q{git2bit migrates issues with their comments and milestones from a github repo to a bitbucket repo}
  gem.homepage      = "https://bitbucket.org/joeworkman/git2bit"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
  gem.add_development_dependency('rdoc')
  gem.add_development_dependency('aruba')
  gem.add_development_dependency('rake', '~> 0.9.2')
  gem.add_dependency('methadone', '~> 1.2.4')
  gem.add_dependency('github_api')
  gem.add_dependency('bitbucket_rest_api')
end
